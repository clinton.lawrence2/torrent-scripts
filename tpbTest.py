from TPB import TPB
from TPB.constants import CATEGORIES
import PTN
import unidecode


torrentname = "Interstellar (2014) (2014) [1080p]"
torrentname = unidecode.unidecode(torrentname)
parsedTitle = PTN.parse(torrentname)
if "year" in parsedTitle.keys():
    cleanTitle = parsedTitle["title"] + " "+ str(parsedTitle["year"])
elif "title" in parsedTitle.keys():
    cleanTitle = parsedTitle["title"]
else:
    cleanTitle = torrentname

t = TPB() # create a TPB object with default domain
print(t)

hits = t.searchCount(cleanTitle)
numMovieHits = hits["Movie"]
numTvHits = hits["TV"]

print(numMovieHits)
print(numTvHits)
print("IsMovie:"+str( numMovieHits > numTvHits))
print("IsTV:"+str(numTvHits > numMovieHits))
remoteLocation = ""


if numMovieHits > numTvHits:
    remoteLocation = "/MOVIES/"
elif numTvHits > numMovieHits:
    remoteLocation = "/TV/"+cleanTitle+"/"
else:
    print("Unable to Categorize Torrent with Name:"+cleanTitle)
    raise Exception("Unable to Categorize Torrent with Name:"+cleanTitle)

print(remoteLocation)
