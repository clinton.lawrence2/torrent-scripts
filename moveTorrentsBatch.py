import moveTorrentsSingle
from utorrentapi import UTorrentAPI,TorrentListInfo

    
utorrentURL = "http://localhost:8080/gui" 
uTorrentUser = "admin"
uTorrentPass = "u!kgeyA2"
apiclient = None
try:
    print("Connecting to UTorrent")
    apiclient = UTorrentAPI(utorrentURL, uTorrentUser, uTorrentPass)
except Exception as err:
    print("Unable to connect to UTorrent")

data = apiclient.get_torrents()
print("Getting all torrents")
tor_list = TorrentListInfo(data)

for torrent in tor_list.torrents:
    print("NAME:"+torrent.name+" Category:"+torrent.label)
    if torrent.percent_progress==1000 and not torrent.name == "":
        moveTorrentsSingle.mainWithCat(torrent.name,torrent.label)
print("Moved All Torrents")