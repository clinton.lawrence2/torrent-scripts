#!/usr/bin/env python
# -*- coding: utf8 -*-

import os
import re
import paramiko
import stat
import errno
import logging
import requests
from bs4 import BeautifulSoup
import sys
from utorrentapi import UTorrentAPI,TorrentListInfo
# we import the Twilio client from the dependency we just installed
import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import argparse
import yaml
import csv
from googleDrive import get_drive_service
from googleapiclient.http import MediaFileUpload
from TPB import TPB
from TPB.constants import CATEGORIES
import PTN
import unidecode


# ' Format of the token array is
    # '   0 = HASH (string),
    # '   1 = STATUS* (integer),
    # '   2 = NAME (string),
    # '   3 = SIZE (integer in bytes),
    # '   4 = PERCENT PROGRESS (integer in per mils),
    # '   5 = DOWNLOADED (integer in bytes),
    # '   6 = UPLOADED (integer in bytes),
    # '   7 = RATIO (integer in per mils),
    # '   8 = UPLOAD SPEED (integer in bytes per second),
    # '   9 = DOWNLOAD SPEED (integer in bytes per second),
    # '   10 = ETA (integer in seconds),
    # '   11 = LABEL (string),
    # '   12 = PEERS CONNECTED (integer),
    # '   13 = PEERS IN SWARM (integer),
    # '   14 = SEEDS CONNECTED (integer),
    # '   15 = SEEDS IN SWARM (integer),
    # '   16 = AVAILABILITY (integer in 1/65535ths),
    # '   17 = TORRENT QUEUE ORDER (integer),
    # '   18 = REMAINING (integer in bytes)

    # ' The ETA (token 10) can have three values:
    # '   -1 = The download has stalled (reported as "infinite" in the UI)
    # '    0 = The download has completed.
    # '   >0 = The number of seconds left.
    # '
    # ' However, the ETA also includes seeding so we need to also ensure that the percentage
    # ' complete is less than 1000 (100%).

currentDir = os.path.dirname(os.path.realpath(__file__))

logLevel = logging.INFO

logPath = os.path.join(currentDir,"moveTorrent.log")
logger = logging.getLogger(__name__)
logger.setLevel(logLevel)

print("Got Logger:"+str(logger))

# create file handler which logs even debug messages
fh = logging.FileHandler(logPath)
fh.setLevel(logLevel)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logLevel)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)


filesToUpload = [".avi",".mp4", ".mkv",".srt"]



def createSSHClient(server, port, user, password):
    # Open a transport
    transport = paramiko.Transport((server,port))
    transport.connect(None,user,password)
    return transport

# Define progress callback that logger.infos the current percentage completed for the file
def progress(sent,size):
    percentage = float(sent)/float(size)*100
    remainderINFO = (percentage) % 10
    remainderDEBUG = (percentage) % 1
    if (remainderINFO > 0 and remainderINFO < .01 ):
        logger.info("Progress: %.2f%%   \r" % (percentage) )
    if (remainderDEBUG > 0 and remainderDEBUG < .01):
        logger.debug("Progress: %.2f%%   \r" % (percentage) )

def mkdirRemote(sftp, remote_directory):
    ##Change to this directory, recursively making new folders if needed.
    ##Returns True if any folders were created.
    if remote_directory == '/':
        # absolute path so change directory to root
        sftp.chdir('/')
        return
    if remote_directory == '':
        # top-level relative directory must exist
        return
    try:
        sftp.chdir(remote_directory) # sub-directory exists
    except IOError:
        dirname, basename = os.path.split(remote_directory.rstrip('/'))
        mkdirRemote(sftp, dirname) # make parent directories
        sftp.mkdir(basename) # sub-directory missing, so created it
        sftp.chdir(basename)
        return True

def sendSMSAlert(localPath):
    smtp_server = "smtp.gmail.com"
    port = 465 
    sender_email = "clinton.lawrence2@gmail.com"
    receiver_email = "6026392118@tmomail.net"
    password = "ohpdthoawarrwysh"
    msg = MIMEMultipart()

    msg['From'] = sender_email
    msg['To'] = receiver_email
    msg['Subject'] = "Torrent Stuff"

    body = "Finished Moving Torrent:"+str(localPath)

    msg.attach(MIMEText(body, 'plain'))
    # Create a secure SSL context
    context = ssl.create_default_context()

    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, msg.as_string())

def mainWithCat(torrentName,category):
    logger.info("Starting to Move finished torrent:"+torrentName+"; Category is "+category)
    
    utorrentURL = "http://localhost:8080/gui" 
    uTorrentUser = "admin"
    uTorrentPass = "u!kgeyA2"
    apiclient = None
    try:
        logger.info("Connecting to UTorrent")
        apiclient = UTorrentAPI(utorrentURL, uTorrentUser, uTorrentPass)
    except Exception as err:
        logger.error("Unable to connect to UTorrent")
    

    piServer = "192.168.86.118"
    piPort = 22
    piUser = "pi"
    piPassword = "u!kgeyA2"
    transport = None
    sftp = None

    try:
        logger.info("Connecting to Pi...")
        transport = createSSHClient(piServer, piPort, piUser, piPassword)
    except Exception as err:
        logger.error("Unable to Connect to PI:"+str(err))
        raise Exception("Unable to Connect to PI:"+str(err))
    try:
        logger.info("Creating SFTP client...")
        sftp = paramiko.SFTPClient.from_transport(transport)
    except Exception as err:
        logger.error("Unable to create SFTP client:"+str(err))
        raise Exception("Unable to create SFTP client:"+str(err))

    remoteDirPrefix = "/mnt/mydisk2TB"

    data = apiclient.get_torrents()
    ##data = apiclient.get_test_torrents()
    logger.info("Getting all torrent to find ours and get files...")
    tor_list = TorrentListInfo(data)
    logger.debug("AMount of TOrrents found:"+str(len(tor_list.torrents)))
    if len(tor_list.torrents) > 0:
        for torrent in tor_list.torrents:
            logger.info("Searching for our torrent in uTOrrent")
            logger.debug(torrent.name)
            logger.debug(torrentName)
            if not torrentName in torrent.name:
                continue
            finished = (torrent.percent_progress==1000)
            logger.debug("Finished:"+str(finished))
            parsedTitle = PTN.parse(torrentName)
            if "title" in parsedTitle.keys():
                cleanTitle = parsedTitle["title"]
            else:
                cleanTitle = torrentName
            if finished:
                if category == "MOVIES":
                    remoteLocation = "/MOVIES/"
                elif category == "TV":
                    remoteLocation = "/TV/"+cleanTitle+"/"
                else:
                    logger.error("Wrong category given, Must be TV OR MOVIES")
                    raise Exception("Wrong category given, Must be TV OR MOVIES")
                logger.debug("Remote Location:"+remoteLocation)
                files = apiclient.get_files(torrent.hash)
                ##files = apiclient.get_test_files()
                logger.debug("FILES JSON:"+str(files))
                savePath = torrent.save_dir      
                logger.debug("FILES:"+str(files['files'][1]))
                for file in files['files'][1]:
                    logger.debug("FileName:"+str(file[0]))
                    videoFile = bool([ele for ele in filesToUpload if(ele in file[0].lower())])
                    logger.debug("VideoFile:"+str(videoFile))
                    if not videoFile:
                        continue
                    remotePath = remoteDirPrefix+remoteLocation+file[0].replace("\\","/")
                    remoteDir = remotePath.rsplit("/",1)[0] ## get everything but the file
                    logger.debug("RemoteDir:"+remoteDir)
                    logger.debug("SAVE:"+savePath)
                    localFilePath = savePath+"\\"+file[0]
                    logger.debug("LocalPath:"+str(localFilePath))
                    if os.path.exists(localFilePath):
                        logger.info("Uploading " + localFilePath + " to " + remotePath )
                        try:
                            logger.info("Seeing if "+ remoteDir+" exists")
                            sftp.chdir(remoteDir)  # Test if remotePath exists
                            logger.info(remoteDir+" Exists")
                        except IOError:
                            logger.info(remoteDir+" Doesn't exist, creating...")
                            mkdirRemote(sftp,remoteDir)  # Create remotePath dir
                        sftp.put(localFilePath,remotePath, callback = progress)
                    else:
                        logger.info("File "+savePath+" already deleted")
                apiclient.removedata(torrent.hash)
                sendSMSAlert(torrent.name)
            else:
                logger.info("Torrent "+torrent.name+" not done yet")
    else:
        logger.info("No Torrents to Move")
    
    # Close
    if sftp: sftp.close()
    if transport: transport.close()

def main():    
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-t", "--torrent", default = "all", help="Torrent Name")
    ap.add_argument("-c", "--category", default = "", help="Torrent Category")
    args = vars(ap.parse_args())    

    logger.debug("Args:"+str(args))

    mainWithCat(args["torrent"],args["category"])

if __name__ == "__main__":  
    main()
