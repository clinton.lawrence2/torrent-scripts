from utorrentapi import UTorrentAPI,TorrentListInfo
import yaml
from googleDrive import get_drive_service
import io
from googleapiclient.http import MediaIoBaseDownload
import csv
import codecs
import os
import logging
import time
from googleDrive import get_drive_service
from googleapiclient.http import MediaFileUpload


fileName = "Torrents"
currentDir = os.path.dirname(os.path.realpath(__file__))

logPath = os.path.join(currentDir,"downloadTorrent.log")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
print("Got Logger:"+str(logger))

# create file handler which logs even debug messages
fh = logging.FileHandler(logPath)
fh.setLevel(logging.INFO)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)

csvPath = r"C:\Users\123fa\torrentScripts\Torrents.csv"

def getFile(driveService):
    fileName = "Torrents"
    service = driveService
    results = None
    fileId = None
    try:
        results = service.files().list(
         q="name='"+fileName+"'",
         fields='files(name,id)').execute()
       
    except Exception as err:
        logging.error("Unable to Get Google Sheets files:"+str(err))
        raise Exception("Unable to Get Google Sheets files:"+str(err))

    items = results.get('files', [])
    if not items:
        logging.info('No files found')
        raise Exception('No file found')
    else:
        logger.info("Downloading File..")
        for item in items:
            fileId = item['id']
            logging.info(fileId)
        try:
            request = service.files().export_media(fileId=fileId,mimeType='text/csv')
            response = request.execute()
            logger.debug("Response Google"+str(response))
            with open(csvPath, "wb") as wer:
                wer.write(response)
        except Exception as err:
            logger.error("Unable to get excel file:"+str(err))
    return fileId

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

def addToUtorrent():
    addedTorrent = False
    with open(csvPath,"r") as csvFile:
        reader = csv.DictReader(csvFile)
        for index,row in enumerate(reader):
            try:
                if row["Magnet"] is None:
                    logger.info("Empty row")
                    break
                utorrentURL = "http://localhost:8080/gui" 
                uTorrentUser = "admin"
                uTorrentPass = "u!kgeyA2"
                apiclient = UTorrentAPI(utorrentURL, uTorrentUser, uTorrentPass)
                torHash = find_between(row["Magnet"],"magnet:?xt=urn:btih:","&dn=").replace("\n","")
                print("TOR HASH:"+torHash)
                apiclient.add_url_with_label(row["Magnet"],torHash,row["Category"])
                addedTorrent = True
            except Exception as err:
                logger.error("Unable to add "+row["Magnet"]+" :"+str(err))
                raise Exception("Unable to add "+row["Magnet"]+" :"+str(err))
        csvFile.close()
    with open(csvPath,'w',newline='') as csvfile:
        fieldnames = ["Category","Magnet"]
        writer = csv.DictWriter(csvfile,fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({"Category":"","Magnet":""})
        csvFile.close()
    return addedTorrent
    

def updateFile(driveService,fileId):
    try:
        logger.info("Upload updated csv to google sheets")
        file_metadata = {
        'name': fileName,
        'mimeType': 'application/vnd.google-apps.spreadsheet'   
        }

        media = MediaFileUpload(r'C:\Users\123fa\torrentScripts\Torrents.csv',
                                mimetype='text/csv',
                                resumable=True)
        file = service.files().update(body=file_metadata,
                                            media_body=media,
                                            fileId=fileId).execute()
    except Exception as err:
        logger.error("Unable to upload changes to google:"+str(err))
        raise Exception("Unable to upload changes to google:"+str(err))


service = None

try:
    logger.info("Getting Google Drive Service")
    service = get_drive_service()
except Exception as err:
    logger.error("Unable to get Google Drive Service")        

fileID = getFile(service)
logger.debug("Sheets File ID:"+str(fileID))
addedTorrent = addToUtorrent()
if addedTorrent:
    updateFile(service,fileID)
else:
    print("There were no torrents to add")

    


